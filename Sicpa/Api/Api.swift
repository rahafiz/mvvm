//
//  Api.swift
//  Sicpa
//
//  Created by A2133 on 01/05/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD

enum ApiMessageStyle: String {
    case inline = "inline"
    case overlay = "overlay"
    case overlayWithAttachment = "overlayWithAttachment"
    case overlayWithImage = "overlayWithImage"
    case none = ""
}

enum ApiError: Error {
    case objectSerialization(reason: String)
    case ruleError(
        errorCode: Int?,
        messageStyle: ApiMessageStyle,
        title: String?,
        subTitle: String?,
        reason: String
    )
    case ruleAttachmentError(
        errorCode: Int?,
        messageStyle: ApiMessageStyle,
        title: String?,
        subTitle: String?,
        reason: String,
        messageAttachment: String?,
        imageUrl: URL?,
        messageButton: String?
    )
    case networkError(reason: String)
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .objectSerialization(reason: let reason):
            return reason
        case .ruleError(errorCode: _, messageStyle: _, title: _, subTitle: _, reason: let reason):
            return reason
        case .ruleAttachmentError(errorCode: _, messageStyle: _, title: _, subTitle: _, reason: let reason, messageAttachment: _, imageUrl: _, messageButton: _):
            return reason
        case .networkError(reason: let reason):
            return reason
        }
    }
    
    public var errorCode: Int? {
        switch self {
        case .objectSerialization(reason: _):
            return nil
        case .ruleError(errorCode: let _errorCode, messageStyle: _, title: _, subTitle: _, reason: _):
            return _errorCode
        case .ruleAttachmentError(errorCode: let _errorCode, messageStyle: _, title: _, subTitle: _, reason: _, messageAttachment: _, imageUrl: _, messageButton: _):
            return _errorCode
        case .networkError(reason: _):
            return nil
        }
    }
}


class CustomServerTrustPolicyManager: ServerTrustPolicyManager {
    
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        // Check if we have a policy already defined, otherwise just kill the connection
        if let policy = super.serverTrustPolicy(forHost: host) {
            //            BSTLogger.shared.debug(policy)
            return policy
        } else {
            return .customEvaluation({ (_, _) -> Bool in
                return false
            })
        }
    }
}

// Network Manager
public let networkManager = NetworkReachabilityManager()

class BSTSessionManager: SessionManager {
    
    override func request(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil) -> DataRequest {
        var customParameters = parameters
        
        if method == .post {
            if customParameters == nil {
                customParameters = [:]
            }
        }
        
        return super.request(
            url,
            method: method,
            parameters: customParameters,
            encoding: encoding,
            headers: headers
        )
    }
}

class Api {
    static let API = Api()
    
    let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        
        let url = URLComponents.init(string: "https://api.nytimes.com/svc/mostpopular/v2")
      
        let host = url?.host ?? ""

        
        /*let array = ServerTrustPolicy.certificates()
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "\(host)": .customEvaluation(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            )
        ]
        
        let serverTrustPolicyManager = CustomServerTrustPolicyManager(
            policies: serverTrustPolicies
        )*/
        
        return BSTSessionManager(
            configuration: configuration,
            serverTrustPolicyManager: nil
        )
    }()
    let acceptableStatusCodes: [Int] = {
        var arr: [Int] = []
        
        arr.append(contentsOf: 200..<403)
        arr.append(contentsOf: 404..<1000)
        
        return arr
    }()
    
    // Check network status and return error message
    static func networkRechabilityMessage(response: DefaultDataResponse) -> String? {
        switch networkManager!.networkReachabilityStatus {
        case .notReachable : return  "Network is not Reachable"
        case .reachable(.ethernetOrWiFi) : return connectionIssueMessage(response: response)
        case .reachable(.wwan) : return connectionIssueMessage(response: response)
        case .unknown : return "Network is not Reachable"
        }
    }

    // If any error will be there after network establishment it will return that message
    static func connectionIssueMessage(response: DefaultDataResponse) -> String? {
        if let urlError = response.error as? URLError {
            if urlError.code == .timedOut {
                return  "Connection Timed Out"
            } else if (urlError.code == .networkConnectionLost || urlError.code == .unknown || urlError.code == .cannotConnectToHost) {
                return "Something went wrong"
            }
        }
        return nil
    }
    
    static func withResult(_ response: DefaultDataResponse) -> Result<JSON> {
        return withResult(true, response)
    }
    
    static func withResult(_ checkStatus: Bool, _ response: DefaultDataResponse) -> Result<JSON> {
        //        guard response.error == nil else {
        //            BSTLogger.shared.debug(response.error!)
        //            return .failure(response.error!)
        //        }
        
        guard let data: Data = response.data else {
            let errorMsg = NSLocalizedString(
                "common_error_content",
                comment: ""
            )
            
            return .failure(ApiError.objectSerialization(reason: errorMsg))
        }
        
        let json: JSON = JSON(data: data)
        
        if checkStatus {
            if response.response?.statusCode != 200 {
                let errorCode = json["errorCode"].int ?? response.response?.statusCode
                var error: String = {
                    if let errorStr = json["messageText"].string {
                        return errorStr
                    } else if let errorStr = networkRechabilityMessage(response: response) {
                        return errorStr
                    }
                    
                    return NSLocalizedString(
                        "common_error_content",
                        comment: ""
                    )
                }()
                let messageTitle: String = json["messageTitle"].string ?? NSLocalizedString(
                    "common_error_header",
                    comment: ""
                )
                let messageSubTitle: String = json["messageSubTitle"].string ?? NSLocalizedString(
                    "common_error_title",
                    comment: ""
                )
                let imageUrl: URL? = json["imageURL"].url //maybe need to append resolutionType
                let messageButton: String? = json["messageButton"].string
                let messageAttachment: String = json["messageAttachment"].string ?? ""
                let messageStyleString = json["messageStyle"].string ?? ApiMessageStyle.none.rawValue
                let messageStyle: ApiMessageStyle = ApiMessageStyle(
                    rawValue: messageStyleString
                    ) ?? ApiMessageStyle.none
                
                // Message style
                error = error.replacingOccurrences(of: "<br/>", with: "\n")
                
                if let _ = response.error ,networkManager?.isReachable == false{
                    return .failure(
                        ApiError.networkError(reason: error)
                    )
                } else if messageStyle == .overlayWithAttachment || messageStyle == .overlayWithImage {
                    return .failure(
                        ApiError.ruleAttachmentError(
                            errorCode: errorCode,
                            messageStyle: messageStyle,
                            title: messageTitle,
                            subTitle: messageSubTitle,
                            reason: error,
                            messageAttachment: messageAttachment,
                            imageUrl: imageUrl,
                            messageButton: messageButton
                        )
                    )
                } else {
                    return .failure(
                        ApiError.ruleError(
                            errorCode: errorCode,
                            messageStyle: messageStyle,
                            title: messageTitle,
                            subTitle: messageSubTitle,
                            reason: error
                        )
                    )
                }
            }
        }
        
        return .success(json)
    }
    
    static func platform() -> String {
        return "ios"
    }
    
    static func sessionAuthHeader() -> HTTPHeaders {
        let apiKey = "dtII05qgWobTJqkBN3WmTQaq1JpY7HGY"
       
        return ["api-key": apiKey]
    }
    
}
