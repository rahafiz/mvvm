//
//  Api+NYT.swift
//  Sicpa
//
//  Created by A2133 on 01/05/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON
import Alamofire

extension Api {
    static func getEmailedArticle(_ handler: @escaping (Result<Articles>) -> Void) -> Request {
        let apiKey = "dtII05qgWobTJqkBN3WmTQaq1JpY7HGY"
        
        return API.sessionManager
            .request(
                "https://api.nytimes.com/svc/mostpopular/v2/emailed/1.json?api-key=\(apiKey)",
                method: .get,
                headers: nil
            )
            .validate(statusCode: API.acceptableStatusCodes)
            .response { response in
                let result = withResult(response)
                let errMsg = NSLocalizedString("common_an_error_occured_msg",
                                               comment: "")
                do {
                    if let value = result.value {
                        let response = try Mapper<Articles>().map(JSONString: value.rawString()!)
                        handler(.success(response))
                    } else if let error = result.error {
                        handler(.failure(error))
                    } else {
                        throw ApiError.objectSerialization(reason: errMsg)
                    }
                } catch let error {
                    handler(.failure(ApiError.objectSerialization(reason: error.localizedDescription)))
                    return
                }
        }
    }
    
    static func getViewedArticle(_ handler: @escaping (Result<Articles>) -> Void) -> Request {
        let apiKey = "dtII05qgWobTJqkBN3WmTQaq1JpY7HGY"
        
        return API.sessionManager
            .request(
                "https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=\(apiKey)",
                method: .get,
                headers: nil
            )
            .validate(statusCode: API.acceptableStatusCodes)
            .response { response in
                let result = withResult(response)
                let errMsg = NSLocalizedString("common_an_error_occured_msg",
                                               comment: "")
                do {
                    if let value = result.value {
                        let response = try Mapper<Articles>().map(JSONString: value.rawString()!)
                        handler(.success(response))
                    } else if let error = result.error {
                        handler(.failure(error))
                    } else {
                        throw ApiError.objectSerialization(reason: errMsg)
                    }
                } catch let error {
                    handler(.failure(ApiError.objectSerialization(reason: error.localizedDescription)))
                    return
                }
        }
    }
    
    static func getSharedArticle(_ handler: @escaping (Result<Articles>) -> Void) -> Request {
        let apiKey = "dtII05qgWobTJqkBN3WmTQaq1JpY7HGY"
        
        return API.sessionManager
            .request(
                "https://api.nytimes.com/svc/mostpopular/v2/shared/1/facebook.json?api-key=\(apiKey)",
                method: .get,
                headers: nil
            )
            .validate(statusCode: API.acceptableStatusCodes)
            .response { response in
                let result = withResult(response)
                let errMsg = NSLocalizedString("common_an_error_occured_msg",
                                               comment: "")
                do {
                    if let value = result.value {
                        let response = try Mapper<Articles>().map(JSONString: value.rawString()!)
                        handler(.success(response))
                    } else if let error = result.error {
                        handler(.failure(error))
                    } else {
                        throw ApiError.objectSerialization(reason: errMsg)
                    }
                } catch let error {
                    handler(.failure(ApiError.objectSerialization(reason: error.localizedDescription)))
                    return
                }
        }
    }
    
    static func getSearchArticle(searchKeyword: String, _ handler: @escaping (Result<Articles>) -> Void) -> Request {
        let apiKey = "dtII05qgWobTJqkBN3WmTQaq1JpY7HGY"
        
        return API.sessionManager
            .request(
                "https://api.nytimes.com/svc/search/v2/articlesearch.json?q=\(searchKeyword)&api-key=\(apiKey)",
                method: .get,
                headers: nil
            )
            .validate(statusCode: API.acceptableStatusCodes)
            .response { response in
                let result = withResult(response)
                let errMsg = NSLocalizedString("common_an_error_occured_msg",
                                               comment: "")
                do {
                    if let value = result.value {
                        let response = try Mapper<Articles>().map(JSONString: value.rawString()!)
                        handler(.success(response))
                    } else if let error = result.error {
                        handler(.failure(error))
                    } else {
                        throw ApiError.objectSerialization(reason: errMsg)
                    }
                } catch let error {
                    handler(.failure(ApiError.objectSerialization(reason: error.localizedDescription)))
                    return
                }
        }
    }

    
}
