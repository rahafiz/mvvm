//
//  Articles.swift
//  Sicpa
//
//  Created by A2133 on 01/05/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import Foundation
import ObjectMapper

struct Articles: ImmutableMappable {
    let status: String?
    let copyright: String?
    let num_results: Int?
    let results: [ArticlesResults]?
    
    init(map: Map) throws {
        status              = try? map.value("status")
        copyright           = try? map.value("copyright")
        num_results         = try? map.value("num_results")
        results             = try? map.value("results")
    }
    
    func mapping(map: Map) {
        status              >>> map["status"]
        copyright           >>> map["copyright"]
        num_results         >>> map["num_results"]
        results             >>> map["results"]
    }
}

struct ArticlesResults: ImmutableMappable {
    let title: String?
    let published_date: String?
    
    init(map: Map) throws {
        title              = try? map.value("title")
        published_date     = try? map.value("published_date")
     
    }
    
    func mapping(map: Map) {
        title               >>> map["title"]
        published_date      >>> map["published_date"]
    }
}


