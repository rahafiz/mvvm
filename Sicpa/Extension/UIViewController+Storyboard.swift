//
//  UIViewController+Storyboard.swift
//  boostapp
//
//  Created by hongmun on 25/04/2017.
//  Copyright © 2017 BOOST. All rights reserved.
//

import UIKit
import STPopup
import Lottie

protocol VCPageViewModelInputs {
    
}

protocol VCPageViewModelOutputs {
    var pageId: Box<String?> { get }
    var pageName: Box<String?> { get }
}

protocol VCPageViewModelType {
    var inputs: VCPageViewModelInputs { get }
    var outputs: VCPageViewModelOutputs { get }
}

class VCPageViewModel: VCPageViewModelInputs, VCPageViewModelOutputs, VCPageViewModelType {
    let pageId: Box<String?> = Box(nil)
    let pageName: Box<String?> = Box(nil)
    
    var inputs: VCPageViewModelInputs { return self }
    var outputs: VCPageViewModelOutputs { return self }
}

extension UIViewController {
    
    func goToForgotPin() {
        let vc = UIViewController.vc("ResetTransactionPin",identifier: nil)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
   
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    static func vc(_ storyboard: String, identifier: String?) -> UIViewController {
        let storyBoardInstance = UIStoryboard(name: storyboard, bundle: nil)
        
        if let _identifier = identifier {
            return storyBoardInstance.instantiateViewController(
                withIdentifier: _identifier
            )
        } else {
            return storyBoardInstance.instantiateInitialViewController()!
        }
    }
    
    func vc(_ storyboard: String? = nil, identifier: String? = nil) -> UIViewController {
        var _storyboard = self.storyboard!
        if storyboard != nil {
            _storyboard = UIStoryboard(name: storyboard!, bundle: nil)
        }
        
        if identifier != nil {
            return _storyboard.instantiateViewController(withIdentifier: identifier!)
        } else {
            return _storyboard.instantiateInitialViewController()!
        }
    }
    
    func addBackButtonInNavItem(barStyle: UIStatusBarStyle = .default,
                                action : ( () -> Void )? = nil) {
        self.actionHandleBlock(action: action)
        
        let img: UIImage? = {
            switch barStyle {
            case .lightContent:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .default:
                return UIImage(named: "back")?
                    .withRenderingMode(.alwaysOriginal)
            case .blackOpaque:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .darkContent:
                return UIImage(named: "back-black")?
                    .withRenderingMode(.alwaysOriginal)
            }
        }()
        let backBtn = UIBarButtonItem(
            image: img,
            style: .plain,
            target: self,
            action: #selector(pop)
        )
        //        backBtn.imageInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        self.navigationItem.leftBarButtonItem = backBtn
    }
    
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var actionList: [UIViewController : (() -> Void)] = [:]
        }
        
        if action != nil {
            __.actionList[self] =  action
        } else {
            //when button pressed , view will unload.
            if __.actionList[self] != nil {
                __.actionList[self]!()
                __.actionList.removeValue(forKey: self)
            }
        }
    }
    
    @objc private func pop() {
        self.actionHandleBlock()
        
        if self.navigationController?.viewControllers.count ?? 0 > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

}
