//
//  Box.swift
//  Sicpa
//
//  Created by A2133 on 20/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import Foundation

class Box<T> {
    typealias Listener = (T) -> Void
    private var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value  = value
    }
    
    func bind(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
