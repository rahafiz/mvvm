//
//  BSTTextInputCell.swift
//  boostapp
//
//  Created by Cheah Bee Kim on 12/14/17.
//  Copyright © 2017 BOOST. All rights reserved.
//

import UIKit
import Library
import Material
import Validator
import IQKeyboardManagerSwift

class BSTTextInputViewModel {
    enum Validation: String {
        case Required = "required"
        case Email = "email"
        case Password = "password"
        case Name = "name"
        case Nickname = "nickname"
        case Mobile = "mobile"
        case Tac = "tac"
        case Regex = "regex"
        case Digit = "digit"
        case Vehicle = "vehicle"
    }
    
    var textInputId: String?
    
    var leftIcon: UIImage?
    var imgUrlString: String?
    
    var rightView: UIView?
    var rightViewMode: UITextField.ViewMode = .always
    
    var numericErrorMessage: String?
    
    
    var title: String? // Not using 
    var placeholder: String?
    var detail: String?
    var error: String?
    var text: String?
    var attributedText: NSAttributedString?
    
    var minimumDigits: Int?
    var maximumDigits: Int?
    
    //
    
    //
    var validations: [Validation]? = []
    var customValidations: [ValidationRuleSet<String>]?
    
    // Animations flag
    var slideIn: Bool = false
    var hideToolBar: Bool = false
    // Flag
    var isSecureText: Bool = false
    var isVisibilityIconButtonEnabled: Bool = true
    var shouldAnimatePlaceholder: Bool = true
    var shouldShowError: Bool = true
    var keyboardType: UIKeyboardType = .default
    var returnKeyType: UIReturnKeyType = .done
    var autoCapitalizationType: UITextAutocapitalizationType = .sentences
    var autocorrectionType: UITextAutocorrectionType = .no
    var spellCheckingType: UITextSpellCheckingType = .no
    
    var allowEditing: Bool = true
    var shouldHighlight: Bool = false
    
    var allowActionOnFocus: Bool = false
    
    //
    var pristine: Bool = true
    
    //
    var prefix: String?
    var dividerColor: UIColor = UIColor.lightGray
    
    // MARK: - Computed value
    var value: String {
        var str = "\(self.text ?? "")"
        
        let flagSkipTrimming = (
            str.count == prefix?.count && str == prefix
        )
        
        if !flagSkipTrimming {
            str = str.trimmingCharacters(
                in: CharacterSet.whitespacesAndNewlines
            )
        }
        
        return str
    }
    
    var csPaddingBottomContainerView: CGFloat = 0
    
    func checkErrors() -> [Error]? {
        var text = self.text ?? ""
        if let prefix = self.prefix {
            if let range = text.range(of: prefix) {
                text.removeSubrange(range)
            }
        }
        text = text.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
        )
        
        
        if let customValidation = self.customValidations {
            for rules in customValidation {
                var result: ValidationResult
                
                result = Validator.validate(input: text,
                                            rules: rules)
                
                switch result {
                case .valid: break
                case .invalid(let failures):
                    return failures
                }
            }
        }
        
        return nil
    }
}

@objc protocol BSTTextInputCellDelegate: class {
    @objc optional func onValidate(cell: BSTTextInputCell)
    @objc optional func textDidChange(cell: BSTTextInputCell,
                                      textField: UITextField)
    @objc optional func didNotBecomeFirstResponse(cell: BSTTextInputCell,
                                                 textField: UITextField)
    @objc optional func didBecomeFirstResponse(cell: BSTTextInputCell,
                                               textField: UITextField)
    @objc optional func didResignFirstResponder(cell: BSTTextInputCell,
                                                textField: UITextField)
    @objc optional func didPressDone(cell: BSTTextInputCell,
                                     textField: UITextField)
    @objc optional func didToggleTextVisibility(cell: BSTTextInputCell,
                                                textField: UITextField)
    @objc optional func animationDidStop(cell: BSTTextInputCell,
                                         anim: CAAnimation,
                                         finished flag: Bool)
    
  
}

class BSTTextInputCell: UITableViewCell {
    
    @IBOutlet weak var textField: TextField!
    @IBOutlet weak var dividerView: UIView!
    @IBOutlet weak var btmLbl: UILabel!
    
    @IBOutlet weak var csPaddingTopTextField: NSLayoutConstraint!
    @IBOutlet weak var csPaddingBtmTextField: NSLayoutConstraint!
    
    @IBOutlet weak var csPaddingBtmContainerView: NSLayoutConstraint!
    
    fileprivate var lblPasswordHint: UILabel = UILabel()
    
    let viewModel: BSTTextInputViewModel = BSTTextInputViewModel()
    weak var delegate: BSTTextInputCellDelegate?
    
    //
    private let rightViewOffsetX: CGFloat = 16.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
        setupListener()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textField.leftView?.removeFromSuperview()
        textField.rightView?.removeFromSuperview()
        
        textField.leftView = nil
        textField.rightView = nil
        textField.text = nil
        textField.isSecureTextEntry = false
        textField.isVisibilityIconButtonEnabled = false
        
        dividerView.backgroundColor = viewModel.dividerColor
    }
    
    // MARK: - Getter
    override open var intrinsicContentSize: CGSize {
        return contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    // Setup
    private func setupUI() {
        selectionStyle = .none
        
        textField.placeholderActiveColor = UIColor.placeholderDefault()
        textField.placeholderNormalColor = UIColor.placeholderDefault()
        textField.isVisibilityIconButtonAutoHandled = false
        textField.isDividerHidden = true
        
        dividerView.backgroundColor = viewModel.dividerColor
        
        textField.font = viewModel.textFont
        textField.placeholderLabel.font = viewModel.placeholderFont
        
        btmLbl.font = UIFont.t21()
    }
    
    private func setupListener() {
        textField.delegate = self
        
        textField.addTarget(self,
                            action: #selector(textFieldDidChange(_:)),
                            for: .editingChanged)
        
        textField.addTarget(self,
                            action: #selector(textFieldEditingDidBegin(_:)),
                            for: .editingDidBegin)
        textField.addTarget(self,
                            action: #selector(textFieldEditingDidEnd(_:)),
                            for: .editingDidEnd)
    }
    
    func setupLeftIcon(_ image: UIImage?) {
        if image != nil {
            let leftView = UIImageView()
            leftView.image = image
            textField.leftView = leftView
        }
    }
    
    func setupCdnImage(_ urlString: String) {
        let customUrl = "\(urlString)/xhdpi"
        if let url = URL(string: customUrl) {
             let _urlRequest = URLRequest.init(url: url)
                let leftView = UIImageView()
                leftView.af_setImage(withURLRequest: _urlRequest)
                textField.leftView = leftView
        }
    }
    
    /*
     Hacky solution to create rightView
     Creating rightView using UIToolbar should in theory
     allows tons of flexibility and reduce certain UI bugs
     such as infinite layoutSubviews invocation due to unknown
     reason in certain scenarios. Currently one of the major known
     encountered issue is during reloadRow and reloadSection. This
     might differ in different versions and devices.
     
     However UIToolbar itself have tons of internal implementations
     of padding/margin that are not accessible through public variable
     which might caused the UI to bugged in certain versions or devices
     */
    func setupRightView(_ rightView: UIView?) {
        if textField.rightViewMode != viewModel.rightViewMode {
            textField.rightViewMode = viewModel.rightViewMode
        }
        
        if let _rightView = rightView {
            rightView?.removeFromSuperview()
            rightView?.sizeToFit()
            
            let toolbar = UIToolbar()
            
            toolbar.items = [
                UIBarButtonItem.init(customView: _rightView)
            ]
            toolbar.setBackgroundImage(UIImage(),
                                       forToolbarPosition: .any,
                                       barMetrics: .default)
            toolbar.setShadowImage(UIImage(), forToolbarPosition: .any)
            toolbar.sizeToFit()
            
            let toolbarFrame = CGRect.init(
                x: rightViewOffsetX,
                y: 0,
                width: _rightView.frame.width + (rightViewOffsetX * 2) + 10,
                height: toolbar.frame.height
            )
            toolbar.frame = toolbarFrame
            let rightView = UIView.init(frame: toolbarFrame)
            rightView.addSubview(toolbar)
            
            textField.rightView = rightView
        } else {
//            textField.rightView = nil
        }
    }
    
    func setupLblPasswordHint() {
        if viewModel.isSecureText && viewModel.isVisibilityIconButtonEnabled {
            guard let visibilityIconButton = textField.visibilityIconButton else { return }
            
            visibilityIconButton.addSubview(lblPasswordHint)
            
//            lblPasswordHint.font = UIFont.nex_t21()
//
//            let password = viewModel.text ?? ""
//            let passwordStrength = PasswordStrength.checkStrength(password: password)
//
//            switch passwordStrength {
//            case .Weak :
//                lblPasswordHint.text = "Weak"
//                lblPasswordHint.textColor = UIColor.primaryRed()
//            case .Moderate:
//                lblPasswordHint.text = "Medium"
//                lblPasswordHint.textColor = UIColor.boostYellow()
//            case .Strong:
//                lblPasswordHint.text = "Strong"
//                lblPasswordHint.textColor = UIColor.boostGreen()
//            case .None:
//                lblPasswordHint.text = "Weak"
//                lblPasswordHint.textColor = UIColor.primaryRed()
            }
            
            lblPasswordHint.sizeToFit()
            let originalLblFrame = lblPasswordHint.frame
            let btnFrame = visibilityIconButton.frame
            let lblFrame = CGRect(
                x: -(originalLblFrame.width - btnFrame.width),
                y: -(originalLblFrame.height),
                width: originalLblFrame.width,
                height: originalLblFrame.height
            )
            lblPasswordHint.frame = lblFrame
        } else {
            lblPasswordHint.removeFromSuperview()
        }
    }
    
    func setupPlaceholder(_ string: String?,
                          shouldAnimate: Bool = true) {
        textField.placeholderVerticalOffset = 10
        textField.placeholder = string
        
        if string?.count ?? 0 > 0 && shouldAnimate {
            let placeholderHeight = textField
                .placeholderLabel
                .intrinsicContentSize
                .height
            
            csPaddingTopTextField.constant = 10 + placeholderHeight
        } else {
            csPaddingTopTextField.constant = 10
        }
        
        if shouldAnimate {
            self.textField.placeholderAnimation = .default
        } else {
            self.textField.placeholderAnimation = .hidden
        }
    }
    
    func setupSecureText(_ flag: Bool, visibilityButtonEnabled: Bool) {
        if flag || flag && visibilityButtonEnabled {
            textField.isSecureTextEntry = flag
            textField.isVisibilityIconButtonEnabled = visibilityButtonEnabled
            
            guard let visibilityIconButton = textField.visibilityIconButton else { return }
            
            visibilityIconButton.addTarget(
                self,
                action: #selector(handleVisibilityIconButton),
                for: .touchUpInside
            )
        }
    }
    
    func setupRightButton() {
        
    }
    
    // MARK: - Listeners
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let prefix = viewModel.prefix {
            if !(textField.text?.hasPrefix(prefix) ?? false) {
                let text = textField.text?.replacingOccurrences(of: prefix,
                                                                with: "") ?? ""
                
                textField.text = "\(prefix)\(text)"
            }
        }
        
        viewModel.text = textField.text
        
        setupLblPasswordHint()
        
        delegate?.textDidChange?(cell: self,
                                 textField: textField)
    }
    
    @objc func textFieldEditingDidBegin(_ textField: UITextField) {
        dividerView.backgroundColor = viewModel.dividerActiveColor
        
        setupLblPasswordHint()
    }
    
    @objc func textFieldEditingDidEnd(_ textField: UITextField) {
        dividerView.backgroundColor = viewModel.dividerColor
    }
    
    @objc func handleVisibilityIconButton() {
        viewModel.isSecureText = !viewModel.isSecureText
        
        textField.isSecureTextEntry = viewModel.isSecureText
        textField.layoutIfNeeded()
        
        let text = viewModel.text
        textField.text = nil
        textField.text = text
        
        delegate?.didToggleTextVisibility?(cell: self, textField: textField)
        
        UIView.transition(
            with: (textField.visibilityIconButton?.imageView)!,
            duration: 0.3,
            options: .transitionCrossDissolve,
            animations: { [weak self] in
                guard let `self` = self else {
                    return
                }
                
                guard let v = self.textField.visibilityIconButton else {
                    return
                }
                
                v.image = self.viewModel.isSecureText ? Icon.visibilityOff?.tint(with: v.tintColor.withAlphaComponent(0.54)) : Icon.visibility?.tint(with: v.tintColor.withAlphaComponent(0.54))
        })
    }
    
    @objc func nextBtnPressed() {
        self.validate(flagShowError: viewModel.shouldShowError)
        
        if let tableView = findTableView() {
            CATransaction.begin()
            
            CATransaction.setCompletionBlock({
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
                    UIView.performWithoutAnimation {
                        tableView.beginUpdates()
                        tableView.endUpdates()
                    }
                })
            })
            
            IQKeyboardManager.shared.goNext()
            
            CATransaction.commit()
        }
        //        var superview = self.textField.superview
        //
        //        while (superview?.superview != nil) {
        //            superview = superview?.superview
        //
        //            if let tableView = superview as? TPKeyboardAvoidingTableView {
        //                tableView.focusNextTextField()
        //
        //                break
        //            }
        //        }
    }
    
    @objc func doneBtnPressed() {
        self.validate(flagShowError: viewModel.shouldShowError)
        
        self.delegate?.didResignFirstResponder?(cell: self,
                                                textField: self.textField)
        self.delegate?.didPressDone?(cell: self,
                                     textField: self.textField)
    }
    
    //
    func validate(flagShowError: Bool = true) {
        let errors = viewModel.checkErrors()
        let flagValid = (errors?.count ?? 0 == 0) && viewModel.error == nil
        let flagPrisitine = viewModel.pristine
        let error = viewModel.error ?? errors?.first?.localizedDescription
        let detail = viewModel.detail
        
        if !flagPrisitine && !flagValid {
            if flagShowError {
                btmLbl.text = error
                btmLbl.textColor = UIColor.primaryRed()
            } else {
                btmLbl.text = ""
            }
        } else {
            btmLbl.text = detail
            btmLbl.textColor = UIColor.placeholderColor()
        }
        
        csPaddingBtmTextField.constant = 5
        
        self.delegate?.onValidate?(cell: self)
        
        //        if let tableView = findTableView() {
        //            UIView.performWithoutAnimation {
        //                tableView.beginUpdates()
        //                tableView.endUpdates()
        //            }
        //        }
    }
    
    func addNextButtonOnKeyboard(textField: UITextField,
                                 target: Any?,
                                 action: Selector) {
        addActionButtonOnKeyboard(textField: textField,
                                  text: "Next",
                                  target: target,
                                  action: action)
    }
    
    func addDoneButtonOnKeyboard(textField: UITextField,
                                 target: Any?,
                                 action: Selector) {
        addActionButtonOnKeyboard(textField: textField,
                                  text: "Done",
                                  target: target,
                                  action: action)
    }
    
    func addActionButtonOnKeyboard(textField: UITextField,
                                   text: String?,
                                   target: Any?,
                                   action: Selector) {
        if viewModel.hideToolBar == true {
            textField.inputAccessoryView = UIView()
        } else {
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem = UIBarButtonItem(title: text,
                                                        style: UIBarButtonItem.Style.done,
                                                        target: target,
                                                        action: action)
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            
            textField.inputAccessoryView = doneToolbar
        }
    }
    
//    //Getter
//    private func findTableView() -> UITableView? {
//        var superview = self.textField.superview
//
//        while superview?.superview != nil {
//            superview = superview?.superview
//
//            if let tableView = superview as? UITableView {
//                return tableView
//            }
//        }
//
//        return nil
//    }
}

extension BSTTextInputCell: ValueCell {
    /*
     setupRightView need to be executed after setupSecureText as it is using the same rightView
     thus overriding the view
     */
    func configureWith(value: BSTTextInputViewModel) {
        self.viewModel.textInputId = value.textInputId
        self.viewModel.leftIcon = value.leftIcon
        self.viewModel.rightView = value.rightView
        self.viewModel.rightViewMode = value.rightViewMode
        self.viewModel.title = value.title
        self.viewModel.placeholder = value.placeholder
        self.viewModel.detail = value.detail
        self.viewModel.error = value.error
        self.viewModel.text = value.text
        self.viewModel.attributedText = value.attributedText
        self.viewModel.validations = value.validations
        self.viewModel.customValidations = value.customValidations
        self.viewModel.minimumDigits = value.minimumDigits
        self.viewModel.maximumDigits = value.maximumDigits
        self.viewModel.numericErrorMessage = value.numericErrorMessage
        self.viewModel.imgUrlString = value.imgUrlString
        
        self.viewModel.slideIn = value.slideIn
        
        self.viewModel.textFont = value.textFont
        self.viewModel.textColor = value.textColor
        self.viewModel.placeholderFont = value.placeholderFont
        self.viewModel.hideToolBar = value.hideToolBar
        //
        self.viewModel.isSecureText = value.isSecureText
        self.viewModel.isVisibilityIconButtonEnabled = value.isVisibilityIconButtonEnabled
        self.viewModel.shouldShowError = value.shouldShowError
        self.viewModel.shouldAnimatePlaceholder = value.shouldAnimatePlaceholder
        self.viewModel.keyboardType = value.keyboardType
        self.viewModel.returnKeyType = value.returnKeyType
        self.viewModel.autoCapitalizationType = value.autoCapitalizationType
        self.viewModel.autocorrectionType = value.autocorrectionType
        self.viewModel.spellCheckingType = value.spellCheckingType
        self.viewModel.shouldHighlight = value.shouldHighlight
        self.viewModel.allowEditing = value.allowEditing
        self.viewModel.allowActionOnFocus = value.allowActionOnFocus
        self.viewModel.csPaddingBottomContainerView = value.csPaddingBottomContainerView
        
        //
        self.viewModel.prefix = value.prefix
        
        self.setupPlaceholder(self.viewModel.placeholder,
                              shouldAnimate: viewModel.shouldAnimatePlaceholder)
        
        if let _imgUrlString = viewModel.imgUrlString {
            self.setupCdnImage(_imgUrlString)
        } else {
            self.setupLeftIcon(self.viewModel.leftIcon)
        }
        
//        self.setupRightView(self.viewModel.rightView)
        
        csPaddingBtmContainerView.constant = self.viewModel.csPaddingBottomContainerView
        
        if let rightView = viewModel.rightView {
            if let rightButtonView = rightView as? UIButton {
                rightButtonView.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
                rightButtonView.frame = CGRect(x: CGFloat(textField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
                self.textField.rightView = rightButtonView
            } else {
                self.textField.rightView = rightView
            }
            self.textField.rightViewMode = viewModel.rightViewMode
        }
        
//        self.setupSecureText(
//            self.viewModel.isSecureText,
//            visibilityButtonEnabled: self.viewModel.isVisibilityIconButtonEnabled
//        )
        
        if let prefix = self.viewModel.prefix {
            let text = self.viewModel.text ?? ""
            
            if !text.hasPrefix(prefix) {
                self.viewModel.text = "\(prefix)\(text)"
            }
        }
        
        if let attributedText = self.viewModel.attributedText {
            self.textField.attributedText = attributedText
        } else {
            self.textField.attributedText = nil
            self.textField.text = self.viewModel.text
        }
        
//        self.setupPlaceholder(
//            self.viewModel.placeholder,
//            shouldAnimate: viewModel.shouldAnimatePlaceholder
//        )
        
        self.textField.keyboardType = self.viewModel.keyboardType
        self.textField.autocorrectionType = self.viewModel.autocorrectionType
        self.textField.autocapitalizationType = self.viewModel.autoCapitalizationType
        self.textField.spellCheckingType = self.viewModel.spellCheckingType
        self.textField.isEnabled = viewModel.allowEditing
        
        self.viewModel.pristine = value.pristine
        
//        self.validate(flagShowError: viewModel.shouldShowError)
        
//        if self.viewModel.returnKeyType == .next {
//            addNextButtonOnKeyboard(textField: textField,
//                                    target: self,
//                                    action: #selector(nextBtnPressed))
//        } else {
//            addDoneButtonOnKeyboard(textField: textField,
//                                    target: self,
//                                    action: #selector(doneBtnPressed))
//        }
        
        if self.viewModel.shouldHighlight {
            DispatchQueue.main.async {[weak self] in
                guard let strongTextField = self?.textField else {
                    return
                }
                
                _ = strongTextField.becomeFirstResponder()
                _ = self?.textFieldShouldBeginEditing(strongTextField)
            }
        }
        
        if viewModel.slideIn {
//            self.slideIn(completionDelegate: self)
        }
    }
}

extension BSTTextInputCell: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if !viewModel.allowEditing {
            return false
        }
        
        if viewModel.allowActionOnFocus {
            self.delegate?.didNotBecomeFirstResponse?(cell: self,
                                                      textField: textField)
            return false
        }
        
        viewModel.pristine = false
        
        self.delegate?.didBecomeFirstResponse?(cell: self,
                                               textField: textField)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.validate(flagShowError: viewModel.shouldShowError)
        
        self.textField.layoutSubviews()
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        var flag = true
        
        let oldText = textField.text ?? ""
        let deleteLoc = oldText.count - range.length
        
        let isDelete = range.location == deleteLoc && range.length != 0
        
        if let prefix = viewModel.prefix {
            if oldText == prefix && isDelete {
                flag = false
            } else {
                var newText = oldText
                if let rangeToReplace = Range.init(range, in: oldText) {
                    newText.replaceSubrange(
                        rangeToReplace,
                        with: string
                    )
                }
                
                if !newText.hasPrefix(prefix) {
                    flag = false
                }
            }
        }
        
        if let validations = viewModel.validations {
            if let _ = validations.filter({ $0 == .Vehicle }).first {
                let characterSet = CharacterSet.letters
                let numberSet = CharacterSet.decimalDigits

                if string.rangeOfCharacter(from: characterSet) != nil || string.rangeOfCharacter(from: numberSet) != nil || string == "" {
                    flag = true
                } else {
                    flag = false
                }
            }
        }

        return flag
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if viewModel.returnKeyType == .next {
            nextBtnPressed()
        } else {
            doneBtnPressed()
        }
        
        return true
    }
}

extension BSTTextInputCell: CAAnimationDelegate {
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.delegate?.animationDidStop?(
            cell: self,
            anim: anim,
            finished: flag
        )
    }
}
