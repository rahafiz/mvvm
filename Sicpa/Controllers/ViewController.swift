//
//  ViewController.swift
//  Sicpa
//
//  Created by A2133 on 20/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

protocol ViewControllerViewModelInputs {
    
}

protocol ViewControllerViewModelOutputs {
    
    var listings: Box<[ViewControllerTVCViewModelType]> { get }
    var navTitle: Box<String> { get }
    
    var fetchingListings: Box<Bool> { get }
    
    var emptyListingsIcon: Box<UIImage?> { get }
    var emptyListingsTitle: Box<String> { get }
    var emptyListingsDesc: Box<String> { get }



}

protocol ViewControllerViewModelType {
    var inputs: ViewControllerViewModelInputs { get }
    var outputs: ViewControllerViewModelOutputs { get }
}

class ViewControllerViewModel: ViewControllerViewModelInputs, ViewControllerViewModelOutputs, ViewControllerViewModelType {
    
    let listings: Box<[ViewControllerTVCViewModelType]> = Box([])
    let navTitle: Box<String> = Box("NYT")
    
    let fetchingListings: Box<Bool> = Box(true)
    
    let emptyListingsIcon: Box<UIImage?> = Box(UIImage(named:"empty_listing_icon"))
    let emptyListingsTitle: Box<String> = Box("Empty Listing")
    let emptyListingsDesc: Box<String> = Box("There is no listing")
    

    var inputs: ViewControllerViewModelInputs { return self }
    var outputs: ViewControllerViewModelOutputs { return self }
    
    init() {
        
    }

}


class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: ViewControllerViewModelType = ViewControllerViewModel()
    var dataSource = ViewControllerDS()
    let searchBtn = UIButton()
    
    // MARK: - Cache
    fileprivate var cellHeightCache = [IndexPath: CGFloat]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupView()
        setupListener()
        setupNavBar()

    }

    private func setupView() {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        
    }
    
    private func setupNavBar() {
        searchBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchBtn.setImage(UIImage(named: "search_icon"), for: .normal)
        searchBtn.addTarget(self, action: #selector(goToSearch), for: .touchUpInside)
        searchBtn.backgroundColor = UIColor.clear
        let _searchBtn = UIBarButtonItem(customView: searchBtn)
        
        self.navigationItem.rightBarButtonItem = _searchBtn
        
    }

    
    func setupListener() {
        
        tableView.registerCellNibForClass(ViewControllerTVC.self)
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        viewModel.outputs.listings.bind { [weak self] result in
            guard let strongSelf = self else { return }
            
            DispatchQueue.main.async {
                strongSelf.dataSource.set(popular: result)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
        
        viewModel.outputs.navTitle.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.title = $0
        }
        
        testLoad()
    }
    
    func testLoad() {
        var tempListings = [ViewControllerTVCViewModel]()
        for _ in 0...0 {
            let tempListing = ViewControllerTVCViewModel()
            tempListing.outputs.sectionTitle.value = "Popular"
            
            var tempTitles = [TitleTVCViewModel]()
            for i in 0...2 {
                let tempTitle = TitleTVCViewModel()
                if i == 0 {
                    tempTitle.outputs.title.value = "Most Viewed"
                }
                
                if i == 1 {
                    tempTitle.outputs.title.value = "Most Shared"
                }
                
                if i == 2 {
                    tempTitle.outputs.title.value = "Most Emailed"
                }
                
                tempTitles.append(tempTitle)
            }
            
            tempListing.outputs.specificListings.value = tempTitles
            
            tempListings.append(tempListing)
            
        }
        
        viewModel.outputs.listings.value = tempListings
        
        
    }
    
    @objc func goToSearch() {
        
        let identifier = String(describing: SearchVC.self)
        guard let vc = UIViewController.vc(
            "Main",
            identifier: identifier
            ) as? SearchVC else { return }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func goToResultVC(listing: String) {
        
        let identifier = String(describing: ResultVC.self)
        guard let vc = UIViewController.vc(
            "Main",
            identifier: identifier
            ) as? ResultVC else { return }
        
        if listing == "Most Viewed" {
            vc.viewModel.outputs.resultType.value = .Viewed
        } else if listing == "Most Shared" {
            vc.viewModel.outputs.resultType.value = .Shared
        } else if listing == "Most Emailed" {
            vc.viewModel.outputs.resultType.value = .Emailed
        }
        
        navigationController?.pushViewController(vc, animated: true)

    }

}

extension ViewController: DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return viewModel.outputs.emptyListingsIcon.value
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string:viewModel.outputs.emptyListingsTitle.value, attributes:
            [NSAttributedString.Key.foregroundColor: UIColor.black,
             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string:viewModel.outputs.emptyListingsDesc.value, attributes:
            [NSAttributedString.Key.foregroundColor: UIColor.lightGray,
             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)])
    }
}


extension ViewController: DZNEmptyDataSetDelegate {
    func emptyDataSetShouldBeForced(toDisplay scrollView: UIScrollView!) -> Bool {
        return !viewModel.outputs.fetchingListings.value && viewModel.outputs.listings.value.count == 0

    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}


extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightCache[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ViewControllerTVC {
            cell.delegate = self
            cell.selectionStyle = .none
        }
        
        cellHeightCache[indexPath] = cell.frame.height
    }
}

extension ViewController: ViewControllerTVCDelegate {
    func viewControllerTVC(_ cell: ViewControllerTVC, didClickListing listing: TitleTVCViewModelType) {
        guard let title = listing.outputs.title.value else { return }
        self.goToResultVC(listing: title)
    }
}


