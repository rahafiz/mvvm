//
//  TitleTVC.swift
//  Sicpa
//
//  Created by A2133 on 20/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

protocol TitleTVCViewModelInputs {
    
    
}

protocol TitleTVCViewModelOutputs {
    
    var title: Box<String?> { get }
    
}

protocol TitleTVCViewModelType {
    var inputs: TitleTVCViewModelInputs { get }
    var outputs: TitleTVCViewModelOutputs { get }
}

class TitleTVCViewModel: TitleTVCViewModelInputs, TitleTVCViewModelOutputs, TitleTVCViewModelType {
    
    let title: Box<String?> = Box(nil)
    
    var inputs: TitleTVCViewModelInputs { return self }
    var outputs: TitleTVCViewModelOutputs { return self }
    
}


class TitleTVC: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    let viewModel: TitleTVCViewModelType = TitleTVCViewModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()
        setupNavBar()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupView() {
        
    }
    
    private func setupNavBar() {
        
    }
    
    
    func setupListener() {
        
        viewModel.outputs.title.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.titleLbl.text = $0
        }
    }
}

extension TitleTVC: ValueCell {
    func configureWith(value: TitleTVCViewModelType) {
        viewModel.outputs.title.value = value.outputs.title.value
    }
}

