//
//  ViewControllerTVCDS.swift
//  Sicpa
//
//  Created by A2133 on 21/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

class ViewControllerTVCDS: ValueCellDataSource {
    enum Section: Int {
        case Listing
    }
    override init() {
        super.init()
    }
    
    
    func set(listing: [TitleTVCViewModelType]) {
        let section = Section.Listing.rawValue
        
        self.set(
            values: listing,
            cellClass: TitleTVC.self,
            inSection: section
        )
        
    }
    
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as TitleTVC, value as TitleTVCViewModel):
            cell.configureWith(value: value)
        default:
            assertionFailure("Unrecognized combo: \(cell), \(value)")
        }
    }
}
