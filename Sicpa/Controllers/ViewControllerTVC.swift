//
//  ViewControllerTVC.swift
//  Sicpa
//
//  Created by A2133 on 20/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

protocol ViewControllerTVCViewModelInputs {
    
    
}

protocol ViewControllerTVCViewModelOutputs {
    
    var sectionTitle: Box<String?> { get }
    var specificListings: Box<[TitleTVCViewModelType]> { get }
    
}

protocol ViewControllerTVCViewModelType {
    var inputs: ViewControllerTVCViewModelInputs { get }
    var outputs: ViewControllerTVCViewModelOutputs { get }
}


protocol ViewControllerTVCDelegate: class {
    func viewControllerTVC(
        _ cell: ViewControllerTVC,
        didClickListing listing: TitleTVCViewModelType
    )
}


class ViewControllerTVCViewModel: ViewControllerTVCViewModelInputs, ViewControllerTVCViewModelOutputs, ViewControllerTVCViewModelType {
    
    let sectionTitle: Box<String?> = Box(nil)
    let specificListings: Box<[TitleTVCViewModelType]> = Box([])
    
    var inputs: ViewControllerTVCViewModelInputs { return self }
    var outputs: ViewControllerTVCViewModelOutputs { return self }
    
}

class ViewControllerTVC: UITableViewCell {
    @IBOutlet weak var sectionLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: ViewControllerTVCViewModelType = ViewControllerTVCViewModel()
    var dataSource = ViewControllerTVCDS()
    weak var delegate: ViewControllerTVCDelegate?
    
    // MARK: - Cache
    fileprivate var cellHeightCache = [IndexPath: CGFloat]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()
        setupNavBar()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupView() {
        //tableView.estimatedRowHeight = UITableView.automaticDimension
        //tableView.rowHeight = UITableView.automaticDimension
        //tableView.separatorStyle = .none
        
    }
    
    private func setupNavBar() {
        
    }
    
    
    func setupListener() {
        
        tableView.registerCellNibForClass(TitleTVC.self)
        tableView.delegate = self
        tableView.dataSource = dataSource
        
        viewModel.outputs.specificListings.bind { [weak self] result in
            guard let strongSelf = self else { return }
            
            strongSelf.dataSource.set(listing: result)
            strongSelf.tableView.reloadData()
            strongSelf.tableView.layoutIfNeeded()
        }
        
        viewModel.outputs.sectionTitle.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.sectionLbl.text = $0
        }
    }

}

extension ViewControllerTVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightCache[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cellHeightCache[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let listings = viewModel.outputs.specificListings.value
        if listings.count > 0 {
            let selectedListing = listings[indexPath.row]
            delegate?.viewControllerTVC(self, didClickListing: selectedListing)
        }
    }

    
}

extension ViewControllerTVC: ValueCell {
    func configureWith(value: ViewControllerTVCViewModelType) {
        viewModel.outputs.specificListings.value = value.outputs.specificListings.value
        viewModel.outputs.sectionTitle.value = value.outputs.sectionTitle.value
    }
}


