//
//  SearchVC.swift
//  Sicpa
//
//  Created by A2133 on 21/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Hero

struct CustomerInformation {
    var search: String
}

protocol SearchVCViewModelInputs{
    
}

protocol SearchVCViewModelOutputs {
    var searchVM: Box<TextInputTVCViewModel> {get}
    var searchButton: Box<SingleBtnTVCViewModel> { get }
    
    var searchKeyword: Box<String?> { get }

}

protocol SearchVCViewModelType {
    var inputs: SearchVCViewModelInputs { get }
    var outputs: SearchVCViewModelOutputs { get }
}

class SearchVCViewModel: SearchVCViewModelInputs, SearchVCViewModelOutputs, SearchVCViewModelType {
    let searchVM: Box<TextInputTVCViewModel>
    let searchButton: Box<SingleBtnTVCViewModel>
    
    let searchKeyword: Box<String?> = Box(nil)
    
    var inputs: SearchVCViewModelInputs { return self }
    var outputs: SearchVCViewModelOutputs { return self }
    
    init() {
        
        let tempNameVM = TextInputTVCViewModel()
        tempNameVM.textInputId = "search"
        tempNameVM.leftIcon = UIImage(named: "search_icon")
        tempNameVM.placeholder = "enter your search"
        searchVM = Box(tempNameVM)
        
        let tempSearchButton = SingleBtnTVCViewModel()
        searchButton = Box(tempSearchButton)

    }
    
}

class SearchVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var cellHeightsDict = [IndexPath: CGFloat]()
    let viewModel: SearchVCViewModelType = SearchVCViewModel()
    var dataSource = SearchVCDS()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupListener()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let titleCell = tableView.cellForRow(at: IndexPath(row: 0, section: SearchVCDS.Section.Input.rawValue)) as? TextInputTVC else {
            return
        }
        
        let titleViewModel = viewModel.outputs.searchVM.value
        
        UIView.animate(
            withDuration: 0.5,
            animations: { [weak self] in
                self?.tableView.beginUpdates()
                
                titleCell.configureWith(value: titleViewModel)
                
                titleCell.layoutIfNeeded()
                
                self?.tableView.endUpdates()
                
        })
    }

    
    func setupView() {
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        
        /*okButton.clipsToBounds = true
        okButton.layer.cornerRadius = okButton.frame.height / 2.0
        okButton.backgroundColor = UIColor.primaryRed()
        okButton.setImage(UIImage(named: "tick-white")?
            .withRenderingMode(.alwaysOriginal),
                          for: .normal)
        
        okButton.isUserInteractionEnabled = false
        okButton.alpha = 0.2*/
    }
    
    func setupListener() {
        
        tableView.registerCellNibForClass(TextInputTVC.self)
        tableView.registerCellNibForClass(SingleBtnTVC.self)
        tableView.dataSource = dataSource
        tableView.delegate = self
        dataSource.registerClasses(tableView: tableView)
        
        viewModel.outputs.searchVM.bind{ [weak self] value in
            DispatchQueue.main.async {
                guard let strongSelf = self else {return}
                strongSelf.dataSource.set(input: [value])
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
        
        viewModel.outputs.searchButton.bind{ [weak self] value in
            DispatchQueue.main.async {
                guard let strongSelf = self else {return}
                strongSelf.dataSource.set(button: [value])
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }

    }
    
    @objc func okButtonTapped() {
    }
    
}

extension SearchVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightsDict[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDict[indexPath] = cell.frame.height

        if let cell = cell as? TextInputTVC {
            cell.delegate = self
            cell.selectionStyle = .none
        } else if let cell = cell as? SingleBtnTVC {
            cell.delegate = self
            cell.selectionStyle = .none

        }
        
        let accumIdx: Int = {
            var sum: Int = indexPath.row
            
            for i: Int in 0..<indexPath.section {
                sum += tableView.numberOfRows(inSection: i)
            }
            
            return sum
        }()
        
        let delay: TimeInterval = TimeInterval(0.1 * CGFloat(accumIdx))
        let easeTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        
        cell.hero.modifiers = [
            HeroModifier.when({ (context) -> Bool in
                return context.toViewController == self
                    && context.isPresenting
            }, HeroModifier.delay(delay),
               HeroModifier.translate(
                CGPoint(x: tableView.frame.width, y: 0)
                ),
               HeroModifier.fade,
               HeroModifier.timingFunction(easeTiming),
               HeroModifier.duration(0.5)
            )
        ]
    }

}

extension SearchVC: TextInputTVCDelegate {
    func didBecomeFirstResponse(cell: TextInputTVC, textField: UITextField) {
        if let indexPath = tableView.indexPath(for: cell) {
    
        }
    }
    
    func didNotBecomeFirstResponse(cell: TextInputTVC, textField: UITextField) {
        view.endEditing(true)
    }
    
    func textDidChange(cell: TextInputTVC, textField: UITextField) {
        guard let text = textField.text else { return }
        viewModel.outputs.searchKeyword.value = text
    }
    
    func didPressDone(cell: TextInputTVC, textField: UITextField) {
        self.view.endEditing(true)
        guard let text = textField.text else { return }
        viewModel.outputs.searchKeyword.value = text
    }
    
    func onValidate(cell: TextInputTVC) {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
}

extension SearchVC: SingleBtnTVCDelegate {
    func didClickSearchBtn() {
        guard let searchKeyword = viewModel.outputs.searchKeyword.value else { return }
        
        let identifier = String(describing: ResultVC.self)
        guard let vc = UIViewController.vc(
            "Main",
            identifier: identifier
            ) as? ResultVC else { return }
        
        let prepareSearchKeyword = searchKeyword.replacingOccurrences(of: " ", with: "+")
        vc.viewModel.outputs.searchKeyword.value = prepareSearchKeyword.lowercased()
        vc.viewModel.outputs.resultType.value = .Search
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

