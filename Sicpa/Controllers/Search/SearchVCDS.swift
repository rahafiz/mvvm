//
//  SearchVCDS.swift
//  Sicpa
//
//  Created by A2133 on 21/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

class SearchVCDS: ValueCellDataSource {
    enum Section: Int {
        case Input
        case Button
    }
    override init() {
        super.init()
    }
    
    
    func set(input: [TextInputTVCViewModel]) {
        let section = Section.Input.rawValue
        
        self.set(
            values: input,
            cellClass: TextInputTVC.self,
            inSection: section
        )
        
    }
    
    func set(button: [SingleBtnTVCViewModel]) {
        let section = Section.Button.rawValue
        
        self.set(
            values: button,
            cellClass: SingleBtnTVC.self,
            inSection: section
        )
        
    }

    
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as TextInputTVC, value as TextInputTVCViewModel):
            cell.configureWith(value: value)
        case let (cell as SingleBtnTVC, value as SingleBtnTVCViewModel):
            cell.configureWith(value: value)
        default:
            assertionFailure("Unrecognized combo: \(cell), \(value)")
        }
    }

}
