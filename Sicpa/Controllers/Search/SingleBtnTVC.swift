//
//  SingleBtnTVC.swift
//  Sicpa
//
//  Created by A2133 on 21/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

protocol SingleBtnTVCViewModelInputs {
    
}

protocol SingleBtnTVCViewModelOutputs {
    
    var btnTitle: Box<String> { get }
    
}

protocol SingleBtnTVCViewModelType {
    var inputs: SingleBtnTVCViewModelInputs { get }
    var outputs: SingleBtnTVCViewModelOutputs { get }
}

protocol SingleBtnTVCDelegate: class {
    func didClickSearchBtn()
}

class SingleBtnTVCViewModel: SingleBtnTVCViewModelInputs, SingleBtnTVCViewModelOutputs, SingleBtnTVCViewModelType {
    
    let btnTitle: Box<String> = Box("Search")
    
    var inputs: SingleBtnTVCViewModelInputs { return self }
    var outputs: SingleBtnTVCViewModelOutputs { return self }
    
}

class SingleBtnTVC: UITableViewCell {

    @IBOutlet weak var searchButton: UIButton!
    let viewModel: SingleBtnTVCViewModelType = SingleBtnTVCViewModel()
    weak var delegate: SingleBtnTVCDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()
        setupNavBar()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupView() {
        
    }
    
    private func setupNavBar() {
        
    }
    
    
    func setupListener() {
        searchButton.addTarget(self, action: #selector(searchBtnTapAction), for: .touchUpInside)

        viewModel.outputs.btnTitle.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.searchButton.setTitle($0, for: .normal)
            
        }
    }
    
    @objc func searchBtnTapAction() {
        delegate?.didClickSearchBtn()
    }
}

extension SingleBtnTVC: ValueCell {
    func configureWith(value: SingleBtnTVCViewModelType) {
        viewModel.outputs.btnTitle.value = value.outputs.btnTitle.value
    }
}

