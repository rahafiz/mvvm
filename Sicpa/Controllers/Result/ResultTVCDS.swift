//
//  ResultTVCDS.swift
//  Sicpa
//
//  Created by A2133 on 01/05/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

class ResultTVCDS: ValueCellDataSource {
    enum Section: Int {
        case Results
    }
    override init() {
        super.init()
    }
    
    
    func set(results: [TitleWithDescriptionTVCViewModelType]) {
        let section = Section.Results.rawValue
        
        self.set(
            values: results,
            cellClass: TitleWithDescriptionTVC.self,
            inSection: section
        )
        
    }
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as TitleWithDescriptionTVC, value as TitleWithDescriptionTVCViewModel):
            cell.configureWith(value: value)
        default:
            assertionFailure("Unrecognized combo: \(cell), \(value)")
        }
    }

}
