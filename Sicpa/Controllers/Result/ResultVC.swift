//
//  ResultVC.swift
//  Sicpa
//
//  Created by A2133 on 01/05/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import SVProgressHUD

protocol ResultVCViewModelInputs {
    
}

protocol ResultVCViewModelOutputs {
    var results: Box<[TitleWithDescriptionTVCViewModelType]> { get }
    var resultType: Box<ResultVCViewModel.ResultType> { get }
    var searchKeyword: Box<String?> { get }
    
    var emptyListingsIcon: Box<UIImage?> { get }
    var emptyListingsTitle: Box<String> { get }
    var emptyListingsDesc: Box<String> { get }
    
    var fetchingResults: Box<Bool> { get }

}

protocol ResultVCViewModelType {
    var inputs: ResultVCViewModelInputs { get }
    var outputs: ResultVCViewModelOutputs { get }
}

class ResultVCViewModel: ResultVCViewModelInputs, ResultVCViewModelOutputs, ResultVCViewModelType {
  
    enum ResultType: Int {
        case Emailed
        case Viewed
        case Shared
        case Search
    }

    let results: Box<[TitleWithDescriptionTVCViewModelType]> = Box([])
    let resultType: Box<ResultVCViewModel.ResultType> = Box(.Search)
    let searchKeyword: Box<String?> = Box(nil)

    let emptyListingsIcon: Box<UIImage?> = Box(UIImage(named:"empty_listing_icon"))
    let emptyListingsTitle: Box<String> = Box("Empty Results")
    let emptyListingsDesc: Box<String> = Box("There is no results")
    
    let fetchingResults: Box<Bool> = Box(true)


    var inputs: ResultVCViewModelInputs { return self }
    var outputs: ResultVCViewModelOutputs { return self }
    
}

class ResultVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let viewModel: ResultVCViewModelType = ResultVCViewModel()
    var dataSource = ResultTVCDS()
    
    // MARK: - Cache
    fileprivate var cellHeightCache = [IndexPath: CGFloat]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupNavBar()
        setupListiner()

        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self

    }
    
    func setupNavBar() {
        
    }
    
    private func setupListiner() {
        tableView.registerCellNibForClass(TitleWithDescriptionTVC.self)
        tableView.delegate = self
        tableView.dataSource = dataSource
        
        viewModel.outputs.results.bind { [weak self] result in
            guard let strongSelf = self else { return }
            
            DispatchQueue.main.async {
                strongSelf.dataSource.set(results: result)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
        
        viewModel.outputs.resultType.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            switch $0 {
                case .Emailed:
                    strongSelf.title = "Most Emailed"
                    strongSelf.apiEmailArticle()
                    break
                case .Viewed:
                    strongSelf.title = "Most Viewed"
                    strongSelf.apiViewArticle()
                    break
                case .Shared:
                    strongSelf.title = "Most Shared"
                    strongSelf.apiSharedArticle()
                    break
                case .Search:
                    strongSelf.title = "Search Articles"
                    guard let searchKeyword = strongSelf.viewModel.outputs.searchKeyword.value else { return }
                    strongSelf.apiSearchArticle(searchKeyword: searchKeyword)
                    break
            }
        }
    }
    
    func apiEmailArticle() {
        SVProgressHUD.show()
        viewModel.outputs.fetchingResults.value = true

        _ = Api.getEmailedArticle() {[weak self] (result) in
            SVProgressHUD.dismiss()
            
            guard let strongSelf = self else { return }
            strongSelf.viewModel.outputs.fetchingResults.value = false

            
            if let value = result.value {
                DispatchQueue.main.async {
                    strongSelf.handleGetArticleAPI(response: value)
                }
                
            } else if let error = result.error {
              print("error is \(error.localizedDescription)")
            }
        }
    }
    
    func apiSharedArticle() {
        SVProgressHUD.show()
        viewModel.outputs.fetchingResults.value = true

        _ = Api.getSharedArticle() {[weak self] (result) in
            SVProgressHUD.dismiss()
            
            guard let strongSelf = self else { return }
            strongSelf.viewModel.outputs.fetchingResults.value = false

            
            if let value = result.value {
                DispatchQueue.main.async {
                    strongSelf.handleGetArticleAPI(response: value)
                }
                
            } else if let error = result.error {
                print("error is \(error.localizedDescription)")
            }
        }
    }
    
    func apiViewArticle() {
        SVProgressHUD.show()
        viewModel.outputs.fetchingResults.value = true

        _ = Api.getViewedArticle() {[weak self] (result) in
            SVProgressHUD.dismiss()
            
            guard let strongSelf = self else { return }
            strongSelf.viewModel.outputs.fetchingResults.value = false

            
            if let value = result.value {
                DispatchQueue.main.async {
                    strongSelf.handleGetArticleAPI(response: value)
                }
                
            } else if let error = result.error {
                print("error is \(error.localizedDescription)")
            }
        }
    }
    
    func apiSearchArticle(searchKeyword: String) {
        SVProgressHUD.show()
        viewModel.outputs.fetchingResults.value = true

        _ = Api.getSearchArticle(searchKeyword: searchKeyword) {[weak self] (result) in
            SVProgressHUD.dismiss()

            guard let strongSelf = self else { return }
            strongSelf.viewModel.outputs.fetchingResults.value = false

            
            if let value = result.value {
                DispatchQueue.main.async {
                    strongSelf.handleGetArticleAPI(response: value)
                }
                
            } else if let error = result.error {
                print("error is \(error.localizedDescription)")
            }
        }
    }

    
    func handleGetArticleAPI(response: Articles) {
        guard let results = response.results else { return }
        var tempResults = [TitleWithDescriptionTVCViewModel]()
        for result in results {
            let tempResult = TitleWithDescriptionTVCViewModel()
            tempResult.outputs.title.value = result.title
            tempResult.outputs.description.value = result.published_date
     
            tempResults.append(tempResult)
        }
        viewModel.outputs.results.value = tempResults
    }

}

extension ResultVC: DZNEmptyDataSetSource {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return viewModel.outputs.emptyListingsIcon.value
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string:viewModel.outputs.emptyListingsTitle.value, attributes:
            [NSAttributedString.Key.foregroundColor: UIColor.black,
             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14)])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string:viewModel.outputs.emptyListingsDesc.value, attributes:
            [NSAttributedString.Key.foregroundColor: UIColor.lightGray,
             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)])
    }
}


extension ResultVC: DZNEmptyDataSetDelegate {
    func emptyDataSetShouldBeForced(toDisplay scrollView: UIScrollView!) -> Bool {
        return !viewModel.outputs.fetchingResults.value && viewModel.outputs.results.value.count == 0
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}


extension ResultVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightCache[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        if let cell = cell as? HistorySuperShakeTVC {
        //            cell.delegate = self
        //        }
        
        cellHeightCache[indexPath] = cell.frame.height
    }
}
