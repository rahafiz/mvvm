//
//  ViewControllerDS.swift
//  Sicpa
//
//  Created by A2133 on 20/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

class ViewControllerDS: ValueCellDataSource {
    enum Section: Int {
        //        case Title // title header super shake
        case Popular
        
    }
    override init() {
        super.init()
    }
    
 
    func set(popular: [ViewControllerTVCViewModelType]) {
        let section = Section.Popular.rawValue
        
        self.set(
            values: popular,
            cellClass: ViewControllerTVC.self,
            inSection: section
        )
        
    }
    
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as ViewControllerTVC, value as ViewControllerTVCViewModel):
            cell.configureWith(value: value)
        default:
            assertionFailure("Unrecognized combo: \(cell), \(value)")
        }
    }
}
