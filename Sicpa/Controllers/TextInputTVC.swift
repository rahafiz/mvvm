//
//  TextInputTVC.swift
//  Sicpa
//
//  Created by A2133 on 22/04/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

class TextInputTVCViewModel {
    enum Validation: String {
        case Required = "required"
        case Email = "email"
        case Password = "password"
        case Name = "name"
        case Nickname = "nickname"
        case Mobile = "mobile"
        case Tac = "tac"
        case Regex = "regex"
        case Digit = "digit"
        case Vehicle = "vehicle"
    }
    
    var textInputId: String?
    
    var leftIcon: UIImage?
    var imgUrlString: String?
    
    var rightView: UIView?
    var rightViewMode: UITextField.ViewMode = .always
    
    var numericErrorMessage: String?
    
    
    var title: String? // Not using
    var placeholder: String?
    var detail: String?
    var error: String?
    var text: String?
    var attributedText: NSAttributedString?
    
    var minimumDigits: Int?
    var maximumDigits: Int?
    
    //
    
    //
    var validations: [Validation]? = []
    
    // Animations flag
    var slideIn: Bool = false
    var hideToolBar: Bool = false
    // Flag
    var isSecureText: Bool = false
    var isVisibilityIconButtonEnabled: Bool = true
    var shouldAnimatePlaceholder: Bool = true
    var shouldShowError: Bool = true
    var keyboardType: UIKeyboardType = .default
    var returnKeyType: UIReturnKeyType = .done
    var autoCapitalizationType: UITextAutocapitalizationType = .sentences
    var autocorrectionType: UITextAutocorrectionType = .no
    var spellCheckingType: UITextSpellCheckingType = .no
    
    var allowEditing: Bool = true
    var shouldHighlight: Bool = false
    
    var allowActionOnFocus: Bool = false
    
    //
    var pristine: Bool = true
    
    //
    var prefix: String?
    var dividerColor: UIColor = UIColor.lightGray
    
    // MARK: - Computed value
    var value: String {
        var str = "\(self.text ?? "")"
        
        let flagSkipTrimming = (
            str.count == prefix?.count && str == prefix
        )
        
        if !flagSkipTrimming {
            str = str.trimmingCharacters(
                in: CharacterSet.whitespacesAndNewlines
            )
        }
        
        return str
    }
    
    var csPaddingBottomContainerView: CGFloat = 0

}

@objc protocol TextInputTVCDelegate: class {
    @objc optional func textDidChange(cell: TextInputTVC,
                                      textField: UITextField)
    @objc optional func animationDidStop(cell: TextInputTVC,
                                         anim: CAAnimation,
                                         finished flag: Bool)
    

}

extension TextInputTVC: ValueCell {
    /*
     setupRightView need to be executed after setupSecureText as it is using the same rightView
     thus overriding the view
     */
    func configureWith(value: TextInputTVCViewModel) {
        self.viewModel.textInputId = value.textInputId
        self.viewModel.leftIcon = value.leftIcon
        self.viewModel.rightView = value.rightView
        self.viewModel.rightViewMode = value.rightViewMode
        self.viewModel.title = value.title
        self.viewModel.placeholder = value.placeholder
        self.viewModel.detail = value.detail
        self.viewModel.error = value.error
        self.viewModel.text = value.text
        self.viewModel.attributedText = value.attributedText
        self.viewModel.validations = value.validations
        self.viewModel.minimumDigits = value.minimumDigits
        self.viewModel.maximumDigits = value.maximumDigits
        self.viewModel.numericErrorMessage = value.numericErrorMessage
        self.viewModel.imgUrlString = value.imgUrlString
        
        self.viewModel.slideIn = value.slideIn
        
   
        self.viewModel.hideToolBar = value.hideToolBar
        //
        self.viewModel.isSecureText = value.isSecureText
        self.viewModel.isVisibilityIconButtonEnabled = value.isVisibilityIconButtonEnabled
        self.viewModel.shouldShowError = value.shouldShowError
        self.viewModel.shouldAnimatePlaceholder = value.shouldAnimatePlaceholder
        self.viewModel.keyboardType = value.keyboardType
        self.viewModel.returnKeyType = value.returnKeyType
        self.viewModel.autoCapitalizationType = value.autoCapitalizationType
        self.viewModel.autocorrectionType = value.autocorrectionType
        self.viewModel.spellCheckingType = value.spellCheckingType
        self.viewModel.shouldHighlight = value.shouldHighlight
        self.viewModel.allowEditing = value.allowEditing
        self.viewModel.allowActionOnFocus = value.allowActionOnFocus
        self.viewModel.csPaddingBottomContainerView = value.csPaddingBottomContainerView
        
        //
        self.viewModel.prefix = value.prefix
        
        
        //        self.setupRightView(self.viewModel.rightView)
    
        
        if let prefix = self.viewModel.prefix {
            let text = self.viewModel.text ?? ""
            
            if !text.hasPrefix(prefix) {
                self.viewModel.text = "\(prefix)\(text)"
            }
        }
        
        
        
        self.textField.keyboardType = self.viewModel.keyboardType
        self.textField.autocorrectionType = self.viewModel.autocorrectionType
        self.textField.autocapitalizationType = self.viewModel.autoCapitalizationType
        self.textField.spellCheckingType = self.viewModel.spellCheckingType
        self.textField.isEnabled = viewModel.allowEditing
        
        self.viewModel.pristine = value.pristine
        
        if self.viewModel.shouldHighlight {
            DispatchQueue.main.async {[weak self] in
                guard let strongTextField = self?.textField else {
                    return
                }
                
                _ = strongTextField.becomeFirstResponder()
                _ = self?.textFieldShouldBeginEditing(strongTextField)
            }
        }
    }
}

class TextInputTVC: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var dividerView: UIView!
    
    let viewModel: TextInputTVCViewModel = TextInputTVCViewModel()
    weak var delegate: TextInputTVCDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupListener()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupListener() {
        textField.delegate = self
        
        textField.addTarget(self,
                            action: #selector(textFieldDidChange(_:)),
                            for: .editingChanged)
        
        textField.addTarget(self,
                            action: #selector(textFieldEditingDidBegin(_:)),
                            for: .editingDidBegin)
        textField.addTarget(self,
                            action: #selector(textFieldEditingDidEnd(_:)),
                            for: .editingDidEnd)
    }
    
    // MARK: - Listeners
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let prefix = viewModel.prefix {
            if !(textField.text?.hasPrefix(prefix) ?? false) {
                let text = textField.text?.replacingOccurrences(of: prefix,
                                                                with: "") ?? ""
                
                textField.text = "\(prefix)\(text)"
            }
        }
        
        viewModel.text = textField.text
        
        delegate?.textDidChange?(cell: self,
                                 textField: textField)
    }
    
    @objc func textFieldEditingDidBegin(_ textField: UITextField) {
       
    }
    
    @objc func textFieldEditingDidEnd(_ textField: UITextField) {
        dividerView.backgroundColor = viewModel.dividerColor
    }
    
}

extension TextInputTVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //        self.validate(flagShowError: viewModel.shouldShowError)
        
        self.textField.layoutSubviews()
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        var flag = true
        
        let oldText = textField.text ?? ""
        let deleteLoc = oldText.count - range.length
        
        let isDelete = range.location == deleteLoc && range.length != 0
    
        return flag
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}


extension TextInputTVC: CAAnimationDelegate {
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        self.delegate?.animationDidStop?(
            cell: self,
            anim: anim,
            finished: flag
        )
    }
}

