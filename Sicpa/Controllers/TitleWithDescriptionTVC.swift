//
//  TitleWithDescriptionTVC.swift
//  Sicpa
//
//  Created by A2133 on 01/05/2019.
//  Copyright © 2019 Wan Rahafiz. All rights reserved.
//

import UIKit
import Library

protocol TitleWithDescriptionTVCViewModelInputs {
    
    
}

protocol TitleWithDescriptionTVCViewModelOutputs {
    
    var title: Box<String?> { get }
    var titleTextFont: Box<UIFont> { get }
    var titleTextColor: Box<UIColor> { get }
    
    var description: Box<String?> { get }
    var descriptionTextFont: Box<UIFont> { get }
    var descriptionTextColor: Box<UIColor> { get }
    
}

protocol TitleWithDescriptionTVCViewModelType {
    var inputs: TitleWithDescriptionTVCViewModelInputs { get }
    var outputs: TitleWithDescriptionTVCViewModelOutputs { get }
}

class TitleWithDescriptionTVCViewModel: TitleWithDescriptionTVCViewModelInputs, TitleWithDescriptionTVCViewModelOutputs, TitleWithDescriptionTVCViewModelType {
    
    let title: Box<String?> = Box(nil)
    let titleTextFont: Box<UIFont> = Box(UIFont.boldSystemFont(ofSize: 16))
    let titleTextColor: Box<UIColor> = Box(UIColor.black)
    
    let description: Box<String?> = Box(nil)
    let descriptionTextFont: Box<UIFont> = Box(UIFont.systemFont(ofSize: 14))
    let descriptionTextColor: Box<UIColor> = Box(UIColor.black)
    
    
    var inputs: TitleWithDescriptionTVCViewModelInputs { return self }
    var outputs: TitleWithDescriptionTVCViewModelOutputs { return self }
    
}

class TitleWithDescriptionTVC: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    let viewModel: TitleWithDescriptionTVCViewModelType = TitleWithDescriptionTVCViewModel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListiner()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupView() {
        
    }
    
    private func setupListiner() {
        
        viewModel.outputs.title.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.titleLbl.text = $0
        }
        
        viewModel.outputs.titleTextFont.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.titleLbl.font = $0
        }
        
        viewModel.outputs.titleTextColor.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.titleLbl.textColor = $0
        }

        
        viewModel.outputs.description.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.descriptionLbl.text = $0
        }
        
        viewModel.outputs.descriptionTextFont.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.descriptionLbl.font = $0
        }
        
        viewModel.outputs.descriptionTextColor.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.descriptionLbl.textColor = $0
        }

    }
    
}

extension TitleWithDescriptionTVC: ValueCell {
    func configureWith(value: TitleWithDescriptionTVCViewModelType) {
        viewModel.outputs.title.value = value.outputs.title.value
        viewModel.outputs.titleTextFont.value = value.outputs.titleTextFont.value
        viewModel.outputs.titleTextColor.value = value.outputs.titleTextColor.value
        viewModel.outputs.description.value = value.outputs.description.value
        viewModel.outputs.descriptionTextFont.value = value.outputs.descriptionTextFont.value
        viewModel.outputs.descriptionTextColor.value = value.outputs.descriptionTextColor.value
        
    }
}

